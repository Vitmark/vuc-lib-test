#pragma once
#include <doctest.h>

#include <cstddef>
#include <cstdint>

#include "etl/span.h"
#include "vuc/can.hpp"

template <typename T, typename U>
inline void check_spans(etl::span<T> l, etl::span<U> r) {
  const size_t len = (etl::min)(l.size(), r.size());
  for (size_t i = 0; i < len; ++i) {
    CHECK_EQ(l[i], r[i]);
  }
}

inline void check_can_frames(vuc::can::can_frame l, vuc::can::can_frame r) {
  CHECK_EQ(l.id.ident, r.id.ident);
  CHECK_EQ(l.len, r.len);
  check_spans<uint8_t, uint8_t>(l.data, r.data);
}
