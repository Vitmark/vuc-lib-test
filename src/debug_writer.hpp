#pragma once

#include <stddef.h>
#include <stdint.h>

#include <cstdint>

#include "etl/array.h"
#include "etl/span.h"
#include "etl/vector.h"

template <typename T, size_t WSIZE, size_t RSIZE = WSIZE>
class spi_writer {
 public:
  void start() {
    start_count += 1;
    started   = true;
    new_start = true;
  }

  void end() {
    started = false;
  }

  etl::pair<size_t, size_t> write_read(etl::span<const T> data_w, etl::span<T> data_r) {
    return etl::pair{write(data_w), read(data_r)};
  }

  etl::pair<size_t, size_t> write_read(const T& data_w, etl::span<T> data_r) {
    return etl::pair{write(data_w), read(data_r)};
  }

  etl::pair<size_t, size_t> write_read(etl::span<const T> data_w, T& data_r) {
    return etl::pair{write(data_w), read(data_r)};
  }

  etl::pair<size_t, size_t> write_read(const T& data_w, T& data_r) {
    return etl::pair{write(data_w), read(data_r)};
  }

  size_t write(etl::span<const T> data) {
    if (clear_on_write && new_start) {
      write_buffer.clear();
      new_start = false;
    }
    if (data.size() <= write_buffer.available()) {
      write_buffer.insert(write_buffer.end(), data.begin(), data.end());
      return data.size();
    }
    return 0;
  }

  size_t write(etl::span<const T> data, uint16_t address, size_t address_width) {
    if (clear_on_write && new_start) {
      write_buffer.clear();
      new_start = false;
    }
    if (2 <= write_buffer.available()) {
      const etl::array<uint8_t, 2> temp{static_cast<uint8_t>(address >> 8), static_cast<uint8_t>(address)};
      if (address_width == 1) {
        write_buffer.insert(write_buffer.end(), temp.begin() + 1, temp.end());
      } else {
        write_buffer.insert(write_buffer.end(), temp.begin(), temp.end());
      }
      return write(data);
    }
    if (data.size() <= write_buffer.available()) {
      write_buffer.insert(write_buffer.end(), data.begin(), data.end());
      return data.size();
    }
    return 0;
  }

  size_t write(const T& data) {
    return write({&data, 1});
  }

  size_t read(etl::span<T> data) {
    if (data.size() <= read_buffer.size()) {
      for (T& val : data) {
        val = read_buffer.back();
        read_buffer.pop_back();
      }
      return data.size();
    } else {
      size_t buffer_size = read_buffer.size();
      for (size_t i{0}; i < buffer_size; ++i) {
        data[i] = read_buffer.back();
        read_buffer.pop_back();
      }
      return buffer_size;
    }
  }

  size_t read(T& data) {
    return read({&data, 1});
  }

 public:
  etl::vector<T, WSIZE> write_buffer;
  etl::vector<T, RSIZE> read_buffer;
  size_t start_count{0};
  bool clear_on_write{true};
  bool started{false};
  bool new_start{false};
};

template <size_t WSIZE, size_t RSIZE = WSIZE>
class i2c_writer {
 public:
  size_t write(uint16_t i2c_address, etl::span<const uint8_t> data) {
    last_i2c_address = i2c_address;
    if (clear_on_write) {
      write_buffer.clear();
    }
    if (data.size() <= write_buffer.available()) {
      write_buffer.insert(write_buffer.end(), data.begin(), data.end());
      return data.size();
    }
    return 0;
  }

  size_t write_mem(uint16_t i2c_address, etl::span<const uint8_t> data, uint16_t address, size_t address_width) {
    last_i2c_address = i2c_address;
    if (clear_on_write) {
      write_buffer.clear();
    }
    if (2 <= write_buffer.available()) {
      const etl::array<uint8_t, 2> temp{static_cast<uint8_t>(address >> 8), static_cast<uint8_t>(address)};
      if (address_width == 1) {
        write_buffer.insert(write_buffer.end(), temp.begin() + 1, temp.end());
      } else {
        write_buffer.insert(write_buffer.end(), temp.begin(), temp.end());
      }
      return write(i2c_address, data);
    }
    if (data.size() <= write_buffer.available()) {
      write_buffer.insert(write_buffer.end(), data.begin(), data.end());
      return data.size();
    }
    return 0;
  }

  size_t write(uint16_t i2c_address, const uint8_t& data) {
    return write(i2c_address, {&data, 1});
  }

  size_t read(uint16_t i2c_address, etl::span<uint8_t> data) {
    if (data.size() <= read_buffer.size()) {
      for (uint8_t& val : data) {
        val = read_buffer.back();
        read_buffer.pop_back();
      }
      return data.size();
    } else {
      size_t buffer_size = read_buffer.size();
      for (size_t i{0}; i < buffer_size; ++i) {
        data[i] = read_buffer.back();
        read_buffer.pop_back();
      }
      return buffer_size;
    }
  }

  size_t read(uint16_t i2c_address, uint8_t& data) {
    return read(i2c_address, {&data, 1});
  }

 public:
  etl::vector<uint8_t, WSIZE> write_buffer;
  etl::vector<uint8_t, RSIZE> read_buffer;
  uint16_t last_i2c_address;
  bool clear_on_write{true};
};
