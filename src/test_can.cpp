#include <doctest.h>

#include <cstddef>
#include <cstdint>

#include "vuc/can.hpp"

using vuc::can::can_frame;
using vuc::can::can_id;
using vuc::can::canfd_frame;

TEST_CASE("CAN struct offsets") {
  CHECK_EQ(sizeof(can_frame), 16);
  CHECK_EQ(offsetof(can_frame, data), 8);
  CHECK_EQ(offsetof(can_frame, len), 4);

  CHECK_EQ(sizeof(canfd_frame), 72);
  CHECK_EQ(offsetof(canfd_frame, data), 8);
  CHECK_EQ(offsetof(canfd_frame, len), 4);
}

TEST_CASE("CAN ID") {
  using vuc::can::can_id;
  SUBCASE("Construct non extended ID") {
    constexpr uint32_t message_id = 0x03A2;
    {
      const can_id message(message_id);

      CHECK_EQ(message.get_id(), message_id);
      CHECK_FALSE(message.is_extended());
      CHECK(message.is_standard());
      CHECK_FALSE(message.is_remote());
      CHECK(message.is_data());
    }
    {
      static constexpr can_id message(message_id, false, true);

      CHECK_EQ(message.get_id(), message_id);
      CHECK_FALSE(message.is_extended());
      CHECK(message.is_standard());
      CHECK(message.is_remote());
      CHECK_FALSE(message.is_data());
    }
  }

  SUBCASE("Construct Force extended") {
    constexpr uint32_t message_id = 0x03A2;
    {
      const can_id message(message_id, true);

      CHECK_EQ(message.get_id(), message_id);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_standard());
      CHECK_FALSE(message.is_remote());
      CHECK(message.is_data());
    }

    {
      const can_id message(message_id, true, true);

      CHECK_EQ(message.get_id(), message_id);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_standard());
      CHECK(message.is_remote());
      CHECK_FALSE(message.is_data());
    }
  }

  SUBCASE("Concstruct extended ID") {
    constexpr uint32_t message_id = 1 << 11;

    {
      const can_id message(message_id, true);

      CHECK_EQ(message.get_id(), message_id);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_remote());
    }
    {
      const can_id message(message_id, false, true);

      CHECK_EQ(message.get_id(), message_id);
      CHECK(message.is_extended());
      CHECK(message.is_remote());
    }
  }

  SUBCASE("Set ID") {
    constexpr uint32_t message_id_0 = 0x06FF;
    constexpr uint32_t message_id_1 = 0x03A2;
    constexpr uint32_t message_id_2 = 1 << 11;
    {
      can_id message(message_id_0);
      CHECK_EQ(message.get_id(), message_id_0);
      CHECK_FALSE(message.is_extended());
      CHECK_FALSE(message.is_remote());

      can_id& temp = message.set_id(message_id_1);
      CHECK_EQ(&temp, &message);
      CHECK_EQ(message.get_id(), message_id_1);
      CHECK_FALSE(message.is_extended());
      CHECK_FALSE(message.is_remote());

      message.set_id(message_id_2);
      CHECK_EQ(message.get_id(), message_id_2);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_remote());

      message.set_id(message_id_1);
      CHECK_EQ(message.get_id(), message_id_1);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_remote());

      message.set_id(message_id_0, false);
      CHECK_EQ(message.get_id(), message_id_0);
      CHECK_FALSE(message.is_extended());
      CHECK_FALSE(message.is_remote());
      message.set_rtr(false);
      CHECK_EQ(message.get_id(), message_id_0);
      CHECK_FALSE(message.is_extended());
      CHECK_FALSE(message.is_remote());
      message.set_rtr(true);
      CHECK_EQ(message.get_id(), message_id_0);
      CHECK_FALSE(message.is_extended());
      CHECK(message.is_remote());
      message.set_rtr(false);
      CHECK_EQ(message.get_id(), message_id_0);
      CHECK_FALSE(message.is_extended());
      CHECK_FALSE(message.is_remote());

      message.set_id(message_id_1, true, true);
      CHECK_EQ(message.get_id(), message_id_1);
      CHECK(message.is_extended());
      CHECK(message.is_remote());

      message.set_id(message_id_2, false);
      CHECK_EQ(message.get_id(), message_id_2);
      CHECK(message.is_extended());
      CHECK(message.is_remote());

      message.set_id(message.get_id(), false, false);
      CHECK_EQ(message.get_id(), message_id_2);
      CHECK(message.is_extended());
      CHECK_FALSE(message.is_remote());
    }
  }

  SUBCASE("Increment") {
    SUBCASE("Increment ID Standard") {
      can_id base_id{0x300};

      REQUIRE_EQ(base_id.get_id(), 0x300);
      REQUIRE(base_id.is_standard());

      can_id& temp = base_id.increment_id(0x02);
      CHECK_EQ(&temp, &base_id);

      REQUIRE_EQ(base_id.get_id(), 0x302);
      REQUIRE(base_id.is_standard());
      REQUIRE_FALSE(base_id.is_remote());

      base_id.set_id(base_id.get_id(), false, true);

      base_id.increment_id(0x1000);
      REQUIRE_EQ(base_id.get_id(), 0x1302);
      REQUIRE(base_id.is_extended());
      REQUIRE(base_id.is_remote());
    }

    SUBCASE("Increment ID Extended") {
      can_id base_id{0x300, true};

      REQUIRE_EQ(base_id.get_id(), 0x300);
      REQUIRE(base_id.is_extended());

      base_id.increment_id(0x02);

      REQUIRE_EQ(base_id.get_id(), 0x302);
      REQUIRE(base_id.is_extended());
    }

    SUBCASE("With offset Standard") {
      const can_id base_id{0x300, false, true};
      const can_id base_extended{0x301, true};
      const can_id new_from_extended = base_extended.new_with_increment(0x02);
      const can_id new_id            = base_id.new_with_increment(0x02);
      const can_id new_extended      = new_id.new_with_increment(0x1000);

      REQUIRE_EQ(new_id.get_id(), 0x302);
      REQUIRE(new_id.is_standard());
      REQUIRE(new_id.is_remote());

      REQUIRE_EQ(new_extended.get_id(), 0x1302);
      REQUIRE(new_extended.is_extended());
      REQUIRE(new_extended.is_remote());

      REQUIRE_EQ(new_from_extended.get_id(), 0x303);
      REQUIRE(new_from_extended.is_extended());
      REQUIRE_FALSE(new_from_extended.is_remote());
    }
  }
}

TEST_CASE("CAN Frame") {
  const vuc::can::can_data test_data{1, 2};

  can_frame test_frame_1{0x560, 2, {1, 2}};
  CHECK_EQ(test_frame_1.len, 2);
  CHECK_EQ(test_frame_1.get_dlc(), 2);
  CHECK_EQ(test_frame_1.id.get_id(), 0x560);
  CHECK_FALSE(test_frame_1.id.is_extended());
  CHECK_EQ(test_frame_1.data, test_data);

  static constexpr can_frame test_frame_2{{0x560, true}, 9, {1, 2}};
  CHECK_EQ(test_frame_2.len, 9);
  CHECK_EQ(test_frame_2.get_dlc(), 8);
  CHECK_EQ(test_frame_2.id.get_id(), 0x560);
  CHECK(test_frame_2.id.is_extended());
  CHECK_EQ(test_frame_2.data, test_data);
}

TEST_CASE("CANFD Frame") {
  const vuc::can::canfd_data test_data{1, 2};

  canfd_frame test_frame_1{0x560, 2, canfd_frame::fd_flag, {1, 2}};
  CHECK_EQ(test_frame_1.len, 2);
  CHECK_EQ(test_frame_1.get_dlc(), 2);
  CHECK_EQ(test_frame_1.id.get_id(), 0x560);
  CHECK_FALSE(test_frame_1.id.is_extended());
  CHECK_EQ(test_frame_1.data, test_data);

  static constexpr canfd_frame test_frame_2{{0x560, true}, 13, canfd_frame::fd_flag, {1, 2}};
  CHECK_EQ(test_frame_2.len, 13);
  CHECK_EQ(test_frame_2.get_dlc(), 10);
  CHECK_EQ(test_frame_2.id.get_id(), 0x560);
  CHECK(test_frame_2.id.is_extended());
  CHECK_EQ(test_frame_2.data, test_data);

  CHECK_EQ(canfd_frame::dlc_to_len(8), 8);
  CHECK_EQ(canfd_frame::dlc_to_len(14), 48);
  CHECK_EQ(canfd_frame::len_to_dlc(8), 8);
  CHECK_EQ(canfd_frame::len_to_dlc(24), 12);
  static constexpr auto test_dlc = canfd_frame::len_to_dlc(48);
  CHECK_EQ(test_dlc, 14);
  CHECK_EQ(canfd_frame::len_to_dlc(100), 15);
}
