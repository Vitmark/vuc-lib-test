#include <doctest.h>
#include <sys/types.h>

#include <cstddef>
#include <cstdint>

#include "check.hpp"
#include "etl/array.h"
#include "etl/numeric.h"
#include "vuc/can.hpp"
#include "vuc/can_tp.hpp"

namespace can = vuc::can;

constexpr uint8_t padding{0xAA};

TEST_CASE("CAN TP Create frames") {
  can::can_frame frame{{0x100, true, false}, 0, {0}};
  SUBCASE("Single frame") {
    const etl::array<uint8_t, 4> data_to_send{'A', 'B', 'C', 'D'};
    const can::can_data expected_data{0x4, 'A', 'B', 'C', 'D', padding, padding, padding};
    const can::can_frame expected_frame{{0x100, true}, 8, expected_data};

    can::tp::fill_single_frame(frame, data_to_send);
    check_can_frames(frame, expected_frame);
  }

  SUBCASE("Fisrt frame") {
    constexpr auto data_to_send = []() constexpr {
      etl::array<uint8_t, 25> arr{};
      uint8_t data = 0;
      for (auto& elem : arr._buffer) {
        elem = data++;
      }
      return arr;
    }();
    const can::can_data expected_data{1 << 4, 25, 0, 1, 2, 3, 4, 5};
    const can::can_frame expected_frame{{0x100, true}, 8, expected_data};

    can::tp::fill_first_frame(frame, data_to_send);
    check_can_frames(frame, expected_frame);
  }

  SUBCASE("Fisrt frame large") {
    constexpr auto data_to_send = []() constexpr {
      etl::array<uint8_t, 8> arr{};
      uint8_t data = 0;
      for (auto& elem : arr._buffer) {
        elem = data++;
      }
      return arr;
    }();
    const uint16_t overall_data_size = 0x0C82;
    const can::can_data expected_data{1 << 4 | 0xC, 0x82, 0, 1, 2, 3, 4, 5};
    const can::can_frame expected_frame{{0x100, true}, 8, expected_data};

    can::tp::fill_first_frame(frame, data_to_send, overall_data_size);
    check_can_frames(frame, expected_frame);
  }

  SUBCASE("Consecutive frame") {
    const auto data_to_send = []() {
      etl::array<uint8_t, 5> arr{};
      etl::iota(arr.begin(), arr.end(), 10);
      return arr;
    }();
    const uint8_t index = 6;
    const can::can_data expected_data{2 << 4 | index, 10, 11, 12, 13, 14, padding, padding};
    const can::can_frame expected_frame{{0x100, true}, 8, expected_data};

    can::tp::fill_consecutive_frame(frame, data_to_send, index);
    check_can_frames(frame, expected_frame);
  }

  SUBCASE("Flow frame") {
    const can::tp::flow_flag flag = can::tp::flow_flag::Wait;
    const uint8_t block_size      = 5;
    const uint8_t separation_time = 115;
    const can::can_data expected_data{3 << 4 | 1, block_size, separation_time, padding, padding, padding, padding, padding};
    const can::can_frame expected_frame{{0x100, true}, 8, expected_data};

    can::tp::fill_flow_frame(frame, flag, block_size, separation_time);
    check_can_frames(frame, expected_frame);
  }

  SUBCASE("Multiframe") {
    const auto arr = []() {
      etl::array<uint8_t, 25> arr{};
      etl::iota(arr.begin(), arr.end(), 0);
      return arr;
    }();
    const can::can_frame expected_first_frame{{0x100, true}, 8, {1 << 4, 25, 0, 1, 2, 3, 4, 5}};

    can::tp::tx_multiframe multiframe{{0x100, true}, arr};
    REQUIRE_EQ(multiframe.get_remaining_size(), 25);
    REQUIRE_EQ(multiframe.get_sent_size(), 0);
    multiframe.fill_next_frame(frame);
    check_can_frames(frame, expected_first_frame);
    CHECK_EQ(multiframe.get_remaining_size(), 25 - 6);
    CHECK_EQ(multiframe.get_sent_size(), 6);

    uint8_t index = 1;
    can::can_data expected_data{};
    expected_data.fill(padding);
    for (size_t i = 6; i < 20; i += 7) {
      expected_data[0] = can::tp::get_cf_control_byte(index++);
      etl::iota(expected_data.begin() + 1, expected_data.end(), i);
      const can::can_frame expected_frame{{0x100, true}, 8, expected_data};
      multiframe.fill_next_frame(frame);
      check_can_frames(frame, expected_frame);
    }
    CHECK_EQ(multiframe.get_remaining_size(), 5);
    CHECK_EQ(multiframe.get_sent_size(), 20);

    const can::can_frame expected_frame{{0x100, true}, 8, {can::tp::get_cf_control_byte(index++), 20, 21, 22, 23, 24, padding, padding}};
    multiframe.fill_next_frame(frame);
    check_can_frames(frame, expected_frame);
    CHECK_EQ(multiframe.get_remaining_size(), 0);
    CHECK_EQ(multiframe.get_sent_size(), 25);

    // Reset back to first frame
    multiframe.reset();
    multiframe.fill_next_frame(frame);
    check_can_frames(frame, expected_first_frame);
    CHECK_EQ(multiframe.get_remaining_size(), 25 - 6);
    CHECK_EQ(multiframe.get_sent_size(), 6);
  }
}

TEST_CASE("CAN TP Parse frames") {
  const auto data_to_send = []() {
    etl::array<uint8_t, 25> arr{};
    etl::iota(arr.begin(), arr.end(), 0);
    return arr;
  }();
  const etl::span<const uint8_t> data_span{data_to_send};

  etl::array<uint8_t, 30> buffer;
  const etl::span<uint8_t> buffer_span{buffer};

  can::can_frame frame{{0x100, true}, 0, {}};
  SUBCASE("Frame type") {
    can::tp::fill_single_frame(frame, data_span.subspan(0, 4));

    SUBCASE("Correct") {
      const auto ftype = can::tp::parse_frame_type(frame);
      REQUIRE(ftype.has_value());
      CHECK_EQ(ftype.value(), can::tp::frame_type::Single);
    }

    SUBCASE("RTR") {
      frame.id.set_rtr(frame.id.is_data());
      const auto ftype = can::tp::parse_frame_type(frame);
      CHECK(!ftype.has_value());
    }

    SUBCASE("DLC < 8") {
      frame.len -= 1;
      const auto ftype = can::tp::parse_frame_type(frame);
      CHECK(!ftype.has_value());
    }

    SUBCASE("Wrong frame type") {
      frame.data[0] |= 1 << 6;
      const auto ftype = can::tp::parse_frame_type(frame);
      CHECK(!ftype.has_value());
    }
  }

  SUBCASE("Single frame") {
    can::tp::fill_single_frame(frame, data_span.subspan(0, 4));
    size_t data_size;
    REQUIRE(can::tp::parse_single_frame(frame, buffer_span, data_size));
    CHECK_EQ(data_size, 4);
    check_spans(buffer_span.subspan(0, 4), data_span.subspan(0, 4));
  }

  SUBCASE("First frame") {
    can::tp::fill_first_frame(frame, data_span);
    const auto ftype = can::tp::parse_frame_type(frame);
    REQUIRE(ftype.has_value());
    CHECK_EQ(ftype.value(), can::tp::frame_type::First);
    size_t data_size;
    REQUIRE(can::tp::parse_first_frame(frame, buffer_span, data_size));
    CHECK_EQ(data_size, data_span.size());
    check_spans(buffer_span.subspan(0, 6), data_span.subspan(0, 6));
  }

  SUBCASE("Consecutive frame") {
    can::tp::fill_consecutive_frame(frame, data_span, 4);
    const auto ftype = can::tp::parse_frame_type(frame);
    REQUIRE(ftype.has_value());
    CHECK_EQ(ftype.value(), can::tp::frame_type::Consecutive);
    uint8_t index;
    REQUIRE(can::tp::parse_consecutive_frame(frame, buffer_span, index));
    CHECK_EQ(index, 4);
    check_spans(buffer_span.subspan(0, 7), data_span.subspan(0, 7));
  }

  SUBCASE("Flow frame") {
    can::tp::fill_flow_frame(frame, can::tp::flow_flag::Wait, 14, 123);
    const auto ftype = can::tp::parse_frame_type(frame);
    REQUIRE(ftype.has_value());
    CHECK_EQ(ftype.value(), can::tp::frame_type::Flow);
    can::tp::flow_flag flag;
    uint8_t block_size;
    uint8_t separation_time;
    REQUIRE(can::tp::parse_flow_frame(frame, flag, block_size, separation_time));
    CHECK_EQ(flag, can::tp::flow_flag::Wait);
    CHECK_EQ(block_size, 14);
    CHECK_EQ(separation_time, 123);
  }
}
