#include <doctest.h>

#include <cstddef>
#include <cstdint>
#include <iostream>

#include "vuc/debounce.hpp"

TEST_CASE("Debounce") {
  using debounce = vuc::debounce<10, uint32_t>;
  debounce debouncer{};

  CHECK_EQ(debouncer.has_changed(), false);
  CHECK_EQ(debouncer.is_set(), false);
  debouncer.add(false);
  CHECK_EQ(debouncer.has_changed(), false);
  CHECK_EQ(debouncer.is_set(), false);

  for (int i{0}; i < 9; ++i) {
    debouncer.add(true);
    CHECK_EQ(debouncer.has_changed(), false);
    CHECK_EQ(debouncer.is_set(), false);
  }

  debouncer.add(true);
  CHECK_EQ(debouncer.has_changed(), true);
  CHECK_EQ(debouncer.is_set(), true);

  SUBCASE("Different sample") {
    debouncer.add(true);
    CHECK_EQ(debouncer.has_changed(), false);
    CHECK_EQ(debouncer.is_set(), true);
  }

  SUBCASE("Same sample") {
    debouncer.add(false);
    CHECK_EQ(debouncer.has_changed(), false);
    CHECK_EQ(debouncer.is_set(), true);

    for (int i{0}; i < 8; ++i) {
      debouncer.add(false);
      CHECK_EQ(debouncer.has_changed(), false);
      CHECK_EQ(debouncer.is_set(), true);
    }

    debouncer.add(false);
    CHECK_EQ(debouncer.has_changed(), true);
    CHECK_EQ(debouncer.is_set(), false);

    debouncer.add(false);
    CHECK_EQ(debouncer.has_changed(), false);
    CHECK_EQ(debouncer.is_set(), false);
  }
}
