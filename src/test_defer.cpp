#include <doctest.h>

#include <cstddef>
#include <cstdint>
#include <iostream>

#include "vuc/defer.hpp"

TEST_CASE("Defer") {
  size_t counter{0};
  {
    auto temp0 = vuc::defer([&] { counter = counter * 2; });
    auto temp1 = vuc::defer([&] { counter = counter - 2; });
    auto temp2 = vuc::defer([&] { counter = counter + 10; });

    {
      auto lol = vuc::defer([&] { counter += 1; });
    }

    CHECK_EQ(counter, 1);
  }

  CHECK_EQ(counter, 18);

  //   {
  //     auto def_00 = vuc::defer([]{std::cout << "00\n" ;});
  //     auto def_01 = vuc::defer([]{std::cout << "01\n" ;});
  //     auto def_02 = vuc::defer([]{std::cout << "02\n" ;});
  //   }
}
