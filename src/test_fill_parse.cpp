#include <doctest.h>
#include <etl/array.h>

#include <cstddef>
#include <cstdint>

#include "etl/byte_stream.h"
#include "etl/endianness.h"
#include "etl/private/dynamic_extent.h"
#include "vuc/fill_parse.hpp"

template <typename T, typename... Ts>
static constexpr size_t temp_function(T value, Ts... values) {
  return vuc::count_bytes<T, Ts...>::value;
}

TEST_CASE("Count bytes") {
  uint8_t bagr0{0};
  uint16_t bagr1{0};
  float bagr2{0};
  CHECK_EQ(temp_function(bagr0, bagr1, bagr2), sizeof(bagr0) + sizeof(bagr1) + sizeof(bagr2));
  CHECK_EQ(temp_function(bagr1, bagr2), sizeof(bagr1) + sizeof(bagr2));
  CHECK_EQ(temp_function(bagr2), sizeof(bagr2));
}

TEST_CASE("Fill parse with variables") {
  const uint16_t test_val_1 = 0xF5A4;
  const float test_val_2    = 0.268;
  const int16_t test_val_3  = -8456;
  const etl::array<uint8_t, 10> test_val_raw_little{0xA4, 0xF5, 0x4C, 0x37, 0x89, 0x3E, 0xF8, 0xDE};
  const etl::array<uint8_t, 10> test_val_raw_big{0xF5, 0xA4, 0x3E, 0x89, 0x37, 0x4C, 0xDE, 0xF8};
  const size_t all_test_val_size = sizeof(test_val_1) + sizeof(test_val_2) + sizeof(test_val_3);

  SUBCASE("Fill") {
    etl::array<uint8_t, 10> out_array{};

    SUBCASE("Little") {
      etl::byte_stream_writer stream{out_array.begin(), out_array.end(), etl::endian::little};
      REQUIRE_EQ(vuc::fill_stream(stream, test_val_1, test_val_2, test_val_3), all_test_val_size);
      for (size_t i{0}; i < out_array.size(); ++i) {
        CHECK_EQ(out_array[i], test_val_raw_little[i]);
      }
    }

    SUBCASE("Big") {
      etl::byte_stream_writer stream{out_array.begin(), out_array.end(), etl::endian::big};
      REQUIRE_EQ(vuc::fill_stream(stream, test_val_1, test_val_2, test_val_3), all_test_val_size);
      for (size_t i{0}; i < out_array.size(); ++i) {
        CHECK_EQ(out_array[i], test_val_raw_big[i]);
      }
    }

    SUBCASE("More values") {
      etl::byte_stream_writer stream{out_array.begin(), out_array.end(), etl::endian::little};
      CHECK_EQ(vuc::fill_stream(stream, test_val_1, test_val_2, test_val_3, test_val_2), sizeof(test_val_1) + sizeof(test_val_2) + sizeof(test_val_3));
    }
  }

  SUBCASE("Parse") {
    uint16_t out_val_1{0};
    float out_val_2{0};
    int16_t out_val_3{0};

    SUBCASE("Little") {
      etl::byte_stream_reader stream{test_val_raw_little.begin(), test_val_raw_little.end(), etl::endian::little};
      REQUIRE(vuc::parse_stream(stream, out_val_1, out_val_2, out_val_3));
      CHECK_EQ(out_val_1, test_val_1);
      CHECK_EQ(out_val_2, test_val_2);
      CHECK_EQ(out_val_3, test_val_3);
    }

    SUBCASE("Big") {
      etl::byte_stream_reader stream{test_val_raw_big.begin(), test_val_raw_big.end(), etl::endian::big};
      REQUIRE(vuc::parse_stream(stream, out_val_1, out_val_2, out_val_3));
      CHECK_EQ(out_val_1, test_val_1);
      CHECK_EQ(out_val_2, test_val_2);
      CHECK_EQ(out_val_3, test_val_3);
    }

    SUBCASE("More values") {
      etl::byte_stream_reader stream{test_val_raw_little.begin(), test_val_raw_little.end(), etl::endian::little};
      CHECK_FALSE(vuc::parse_stream(stream, out_val_1, out_val_2, out_val_3, out_val_2));
    }
  }
}

TEST_CASE("Fill parse with span") {
  const etl::array<float, 3> test_array{0.268, 6.36e9, 0.548e-6};
  const etl::span<const float> test_array_span{test_array};

  const etl::array<uint8_t, 4 * sizeof(float)> test_val_raw_little{0x4C, 0x37, 0x89, 0x3E, 0xF3, 0x8A, 0xBD, 0x4F, 0x46, 0x1A, 0x13, 0x35};
  const etl::array<uint8_t, 4 * sizeof(float)> test_val_raw_big{0x3E, 0x89, 0x37, 0x4C, 0x4F, 0xBD, 0x8A, 0xF3, 0x35, 0x13, 0x1A, 0x46};

  SUBCASE("Fill") {
    etl::array<uint8_t, 4 * sizeof(float)> out_raw{};

    SUBCASE("Little") {
      etl::byte_stream_writer stream{out_raw.begin(), out_raw.end(), etl::endian::little};
      REQUIRE_EQ(vuc::fill_stream(stream, test_array_span), test_array.size() * sizeof(float));
      for (size_t i{0}; i < out_raw.size(); ++i) {
        CHECK_EQ(out_raw[i], test_val_raw_little[i]);
      }
    }

    SUBCASE("Big") {
      etl::byte_stream_writer stream{out_raw.begin(), out_raw.end(), etl::endian::big};
      REQUIRE_EQ(vuc::fill_stream(stream, test_array_span), test_array.size() * sizeof(float));
      for (size_t i{0}; i < out_raw.size(); ++i) {
        CHECK_EQ(out_raw[i], test_val_raw_big[i]);
      }
    }

    SUBCASE("Bigger array") {
      const etl::array<float, 5> test_array_bigger{};
      const etl::span<const float> test_array_bigger_span{test_array_bigger};
      etl::byte_stream_writer stream{out_raw.begin(), out_raw.end(), etl::endian::native};
      CHECK_EQ(vuc::fill_stream(stream, test_array_bigger_span), 0);
    }
  }

  SUBCASE("Parse") {
    etl::array<float, 3> out_array{};
    SUBCASE("Non dynamic extend") {
      etl::span out_array_span{out_array};

      SUBCASE("Little") {
        etl::byte_stream_reader stream{test_val_raw_little.begin(), test_val_raw_little.end(), etl::endian::little};
        REQUIRE(vuc::parse_stream(stream, out_array_span));
        for (size_t i{0}; i < test_array.size(); ++i) {
          CHECK_EQ(out_array[i], test_array[i]);
        }
      }

      SUBCASE("Big") {
        etl::byte_stream_reader stream{test_val_raw_big.begin(), test_val_raw_big.end(), etl::endian::big};
        REQUIRE(vuc::parse_stream(stream, out_array_span));
        for (size_t i{0}; i < test_array.size(); ++i) {
          CHECK_EQ(out_array[i], test_array[i]);
        }
      }
    }

    SUBCASE("Dynamic extend") {
      etl::span<float> out_array_span{out_array};

      SUBCASE("Little") {
        etl::byte_stream_reader stream{test_val_raw_little.begin(), test_val_raw_little.end(), etl::endian::little};
        REQUIRE(vuc::parse_stream(stream, out_array_span));
        for (size_t i{0}; i < test_array.size(); ++i) {
          CHECK_EQ(out_array[i], test_array[i]);
        }
      }

      SUBCASE("Big") {
        etl::byte_stream_reader stream{test_val_raw_big.begin(), test_val_raw_big.end(), etl::endian::big};
        REQUIRE(vuc::parse_stream(stream, out_array_span));
        for (size_t i{0}; i < test_array.size(); ++i) {
          CHECK_EQ(out_array[i], test_array[i]);
        }
      }
    }

    SUBCASE("Bigger array") {
      etl::array<float, 5> out_array_bigger{};
      etl::span out_array_span{out_array_bigger};
      etl::byte_stream_reader stream{test_val_raw_little.begin(), test_val_raw_little.end(), etl::endian::native};
      CHECK_EQ(vuc::parse_stream(stream, out_array_span), 0);
    }
  }
}
