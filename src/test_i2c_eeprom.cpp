
#include <doctest.h>

#include <cstddef>
#include <cstdint>

#include "debug_writer.hpp"
#include "etl/array.h"
#include "etl/expected.h"
#include "etl/numeric.h"
#include "etl/span.h"
#include "vuc/i2c_eeprom.hpp"
#include "vuc/literals.hpp"

using namespace vuc::literals;

TEST_CASE("I2C EEprom") {
  i2c_writer<200> stream;
  stream.clear_on_write = false;
  vuc::i2c_eeprom<decltype(stream), vuc::i2c_eeprom_size::E24LC64> eeprom(stream, 0x0E);

  SUBCASE("Write single byte") {
    const etl::array<uint8_t, 3> expected_data{0x00, 0x01, 0xAB};

    const etl::array write_data{0xAB_u8};
    const auto result = eeprom.write(1, write_data);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), expected_data.size());
    for (size_t i = 0; i < expected_data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
    CHECK_EQ(stream.last_i2c_address, 0x0E);
  }

  SUBCASE("Write multiple bytes in single page") {
    const etl::array<uint8_t, 6> expected_data{0x02, 0xF1, 0xAB, 0xFB, 0x81, 0x00};

    const etl::array<uint8_t, 4> write_data{0xAB, 0xFB, 0x81, 0x00};
    const auto result = eeprom.write(0x02F1, write_data);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), expected_data.size());
    for (size_t i = 0; i < expected_data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
  }

  SUBCASE("Write byte at page boundary") {
    const etl::array<uint8_t, 6> expected_data{0x00, 31, 0xAB, 0x00, 32, 0x44};

    const etl::array write_data1{0xAB_u8};
    const etl::array write_data2{0x44_u8};
    const auto result = eeprom.write(31, write_data1);
    REQUIRE(result);
    const auto result2 = eeprom.write(32, write_data2);
    REQUIRE(result2);
    REQUIRE_EQ(stream.write_buffer.size(), expected_data.size());
    for (size_t i = 0; i < expected_data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
  }

  SUBCASE("Write till page boundary") {
    const etl::array<uint8_t, 4> expected_data{0x00, 30, 0xAB, 0xFB};
    const etl::array<uint8_t, 4> write_data{0xAB, 0xFB, 0x81, 0x00};

    const auto result = eeprom.write_till_page_boundary(30, write_data);
    REQUIRE(result);
    CHECK_EQ(result.value(), 2);
    CHECK_EQ(stream.write_buffer.size(), expected_data.size());
    for (size_t i = 0; i < (etl::min)(expected_data.size(), stream.write_buffer.size()); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
  }

  SUBCASE("Byte array at page boundary") {
    const etl::array<uint8_t, 8> expected_data{0x00, 30, 0xAB, 0xFB, 0x00, 32, 0x81, 0x00};
    const etl::array<uint8_t, 4> write_data{0xAB, 0xFB, 0x81, 0x00};

    const auto result = eeprom.write(30, write_data);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), expected_data.size());
    for (size_t i = 0; i < expected_data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
  }

  SUBCASE("Data across multiple pages") {
    const size_t expected_write_size = 70 + 4 * 2;
    const auto start_address         = 60_u32;
    const auto write_data            = []() {
      etl::array<uint8_t, 70> out;
      etl::iota(out.rbegin(), out.rend(), 1);
      return out;
    }();

    const auto expected_data = [&]() {
      etl::array<uint8_t, expected_write_size> out{start_address >> 8, start_address & 0xFF};
      size_t address     = start_address;
      size_t write_index = 2;
      for (size_t i{0}; i < write_data.size(); ++i) {
        out[write_index] = write_data[i];
        address += 1;
        if (address % 32 == 0) {
          out[write_index + 1] = address >> 8;
          out[write_index + 2] = address & 0xFF;
          write_index += 3;
        } else {
          write_index += 1;
        }
      }
      return out;
    }();

    const auto result = eeprom.write(start_address, write_data);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), expected_write_size);
    for (size_t i = 0; i < expected_data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_data[i]);
    }
  }

  SUBCASE("Read data") {
    const etl::array<uint8_t, 2> expected_write{0x00, 30};
    const etl::array<uint8_t, 8> read_data{0x00, 30, 0xAB, 0xFB, 0x00, 32, 0x81, 0x00};
    stream.read_buffer.insert(stream.read_buffer.end(), read_data.rbegin(), read_data.rend());

    etl::array<uint8_t, 7> read_buffer{};

    const auto result = eeprom.read(30, read_buffer);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), expected_write.size());
    for (size_t i = 0; i < expected_write.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i], expected_write[i]);
    }
    CHECK_EQ(stream.read_buffer.size(), 1);

    for (size_t i = 0; i < read_buffer.size(); ++i) {
      CHECK_EQ(read_buffer[i], read_data[i]);
    }
  }
}
