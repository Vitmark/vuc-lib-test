
#include <doctest.h>

#include <cstddef>
#include <cstdint>
#include <iostream>

#include "debug_writer.hpp"
#include "etl/array.h"
#include "etl/span.h"
#include "vuc/ltc68042.hpp"

TEST_CASE("LTC6804 CRC") {
  // Code from Analog Devices
  static constexpr unsigned int crc15Table[256] = {
      0x0,    0xc599, 0xceab, 0xb32,  0xd8cf, 0x1d56, 0x1664, 0xd3fd, 0xf407, 0x319e, 0x3aac, 0xff35, 0x2cc8, 0xe951, 0xe263, 0x27fa, 0xad97, 0x680e, 0x633c,
      0xa6a5, 0x7558, 0xb0c1, 0xbbf3, 0x7e6a, 0x5990, 0x9c09, 0x973b, 0x52a2, 0x815f, 0x44c6, 0x4ff4, 0x8a6d, 0x5b2e, 0x9eb7, 0x9585, 0x501c, 0x83e1, 0x4678,
      0x4d4a, 0x88d3, 0xaf29, 0x6ab0, 0x6182, 0xa41b, 0x77e6, 0xb27f, 0xb94d, 0x7cd4, 0xf6b9, 0x3320, 0x3812, 0xfd8b, 0x2e76, 0xebef, 0xe0dd, 0x2544, 0x2be,
      0xc727, 0xcc15, 0x98c,  0xda71, 0x1fe8, 0x14da, 0xd143, 0xf3c5, 0x365c, 0x3d6e, 0xf8f7, 0x2b0a, 0xee93, 0xe5a1, 0x2038, 0x7c2,  0xc25b, 0xc969, 0xcf0,
      0xdf0d, 0x1a94, 0x11a6, 0xd43f, 0x5e52, 0x9bcb, 0x90f9, 0x5560, 0x869d, 0x4304, 0x4836, 0x8daf, 0xaa55, 0x6fcc, 0x64fe, 0xa167, 0x729a, 0xb703, 0xbc31,
      0x79a8, 0xa8eb, 0x6d72, 0x6640, 0xa3d9, 0x7024, 0xb5bd, 0xbe8f, 0x7b16, 0x5cec, 0x9975, 0x9247, 0x57de, 0x8423, 0x41ba, 0x4a88, 0x8f11, 0x57c,  0xc0e5,
      0xcbd7, 0xe4e,  0xddb3, 0x182a, 0x1318, 0xd681, 0xf17b, 0x34e2, 0x3fd0, 0xfa49, 0x29b4, 0xec2d, 0xe71f, 0x2286, 0xa213, 0x678a, 0x6cb8, 0xa921, 0x7adc,
      0xbf45, 0xb477, 0x71ee, 0x5614, 0x938d, 0x98bf, 0x5d26, 0x8edb, 0x4b42, 0x4070, 0x85e9, 0xf84,  0xca1d, 0xc12f, 0x4b6,  0xd74b, 0x12d2, 0x19e0, 0xdc79,
      0xfb83, 0x3e1a, 0x3528, 0xf0b1, 0x234c, 0xe6d5, 0xede7, 0x287e, 0xf93d, 0x3ca4, 0x3796, 0xf20f, 0x21f2, 0xe46b, 0xef59, 0x2ac0, 0xd3a,  0xc8a3, 0xc391,
      0x608,  0xd5f5, 0x106c, 0x1b5e, 0xdec7, 0x54aa, 0x9133, 0x9a01, 0x5f98, 0x8c65, 0x49fc, 0x42ce, 0x8757, 0xa0ad, 0x6534, 0x6e06, 0xab9f, 0x7862, 0xbdfb,
      0xb6c9, 0x7350, 0x51d6, 0x944f, 0x9f7d, 0x5ae4, 0x8919, 0x4c80, 0x47b2, 0x822b, 0xa5d1, 0x6048, 0x6b7a, 0xaee3, 0x7d1e, 0xb887, 0xb3b5, 0x762c, 0xfc41,
      0x39d8, 0x32ea, 0xf773, 0x248e, 0xe117, 0xea25, 0x2fbc, 0x846,  0xcddf, 0xc6ed, 0x374,  0xd089, 0x1510, 0x1e22, 0xdbbb, 0xaf8,  0xcf61, 0xc453, 0x1ca,
      0xd237, 0x17ae, 0x1c9c, 0xd905, 0xfeff, 0x3b66, 0x3054, 0xf5cd, 0x2630, 0xe3a9, 0xe89b, 0x2d02, 0xa76f, 0x62f6, 0x69c4, 0xac5d, 0x7fa0, 0xba39, 0xb10b,
      0x7492, 0x5368, 0x96f1, 0x9dc3, 0x585a, 0x8ba7, 0x4e3e, 0x450c, 0x8095};

  auto pec15_calc = [](uint8_t len, uint8_t* data) -> uint16_t {
    uint16_t remainder, addr;

    remainder = 16;                    // initialize the PEC
    for (uint8_t i = 0; i < len; i++)  // loops for each byte in data array
    {
      addr      = ((remainder >> 7) ^ data[i]) & 0xff;  // calculate PEC table address
      remainder = (remainder << 8) ^ crc15Table[addr];
    }
    return (remainder * 2);  // The CRC15 has a 0 in the LSB so the remainder must be multiplied by 2
  };

  vuc::ltc68042_crc crc_calculator;

#if 0
  for (size_t i{0}; i < 256; ++i) {
    CHECK_EQ(crc_calculator.precomputed[i], crc15Table[i]);
  }
#endif

  etl::array<uint8_t, 5> test_array{0xEE, 0x75, 0x21, 0xA5, 0x38};
  etl::span<uint8_t> test_span{test_array};

  uint16_t accu_crc = 0;
  for (size_t i{0}; i < test_array.size(); ++i) {
    INFO(i);
    const uint16_t crc_result = crc_calculator.calculate(test_span.subspan(0, i + 1));
    CHECK_EQ(pec15_calc(i + 1, test_array.begin()), crc_result);

    if (i == 0) {
      accu_crc = crc_calculator.calculate(test_span.subspan(0, 1));
    } else {
      accu_crc = crc_calculator.accumulate(accu_crc, test_span.subspan(i, 1));
    }
    CHECK_EQ(accu_crc, crc_result);
  }
}

TEST_CASE("LTC6804-2") {
  spi_writer<uint8_t, 20> stream;
  vuc::ltc68042_crc crc;

  vuc::ltc68042 ltc{stream, crc};
  using ltc68042 = decltype(ltc);

  SUBCASE("Start cell measurement 01") {
    const auto result = ltc.start_cell_measurement(false, 1, ltc68042::adc_mode::normal_7khz_3khz, false, ltc68042::cell_selection::all_cells);
    CHECK(result);
    CHECK_FALSE(stream.started);
    REQUIRE_EQ(stream.write_buffer.size(), 4);
    constexpr uint16_t expected_cmd{0b10001'01'10'11'00'000};
    etl::array<uint8_t, 2> temp = {expected_cmd >> 8, expected_cmd & 0xFF};
    const uint16_t expected_pec = crc.calculate(temp);

    uint16_t cmd = stream.write_buffer[0] << 8 | stream.write_buffer[1];
    uint16_t pec = stream.write_buffer[2] << 8 | stream.write_buffer[3];
    CHECK_EQ(cmd, expected_cmd);
    CHECK_EQ(pec, expected_pec);
  }

  SUBCASE("Start cell measurement 02") {
    const auto result = ltc.start_cell_measurement(true, 1, ltc68042::adc_mode::fast_27khz_14khz, true, ltc68042::cell_selection::cell_4_10);
    CHECK(result);
    CHECK_FALSE(stream.started);
    REQUIRE_EQ(stream.write_buffer.size(), 4);
    constexpr uint16_t expected_cmd{0b00000'01'01'11'10'100};
    etl::array<uint8_t, 2> temp = {expected_cmd >> 8, expected_cmd & 0xFF};
    const uint16_t expected_pec = crc.calculate(temp);

    uint16_t cmd = stream.write_buffer[0] << 8 | stream.write_buffer[1];
    uint16_t pec = stream.write_buffer[2] << 8 | stream.write_buffer[3];
    CHECK_EQ(cmd, expected_cmd);
    CHECK_EQ(pec, expected_pec);
  }

  SUBCASE("Start cell measurement write error") {
    stream.write_buffer.uninitialized_resize(stream.write_buffer.max_size() - 3);
    stream.clear_on_write = false;
    const auto result     = ltc.start_cell_measurement(true, 1, ltc68042::adc_mode::fast_27khz_14khz, true, ltc68042::cell_selection::cell_4_10);
    CHECK_FALSE(stream.started);
    CHECK_EQ(stream.write_buffer.size(), stream.write_buffer.max_size() - 3);
    REQUIRE_FALSE(result);
    CHECK_EQ(result.error(), ltc68042::error::spi_write);
  }

  SUBCASE("Read register group pec error") {
    const etl::array<uint8_t, 8> start_array0{0xAC, 0x78, 0xE0, 0x15, 0x68, 0xFF, 0x67, 0xA9};
    stream.read_buffer.insert(stream.read_buffer.end(), start_array0.begin(), start_array0.end());
    auto result = ltc.read_reg_group(1, ltc68042::reg_group_r::cell_voltage_c);
    CHECK_FALSE(stream.started);
    REQUIRE_FALSE(result);
    CHECK_EQ(stream.read_buffer.size(), 0);
    CHECK_EQ(result.error(), ltc68042::error::pec);

    REQUIRE_EQ(stream.write_buffer.size(), 4);
    constexpr uint16_t expected_cmd{0b10001'00000001000};
    etl::array<uint8_t, 2> temp = {expected_cmd >> 8, expected_cmd & 0xFF};
    const uint16_t expected_pec = crc.calculate(temp);

    uint16_t cmd = stream.write_buffer[0] << 8 | stream.write_buffer[1];
    uint16_t pec = stream.write_buffer[2] << 8 | stream.write_buffer[3];
    CHECK_EQ(cmd, expected_cmd);
    CHECK_EQ(pec, expected_pec);
  }

  SUBCASE("Read register group spi read error") {
    const etl::array<uint8_t, 7> start_array0{
        0xAC, 0x78, 0xE0, 0x15, 0x68, 0xFF, 0x67,
    };
    stream.read_buffer.insert(stream.read_buffer.end(), start_array0.begin(), start_array0.end());
    auto result = ltc.read_reg_group(1, ltc68042::reg_group_r::cell_voltage_c);
    CHECK_FALSE(stream.started);
    CHECK_EQ(stream.read_buffer.size(), 0);
    CHECK_EQ(stream.write_buffer.size(), 4);
    REQUIRE_FALSE(result);
    CHECK_EQ(result.error(), ltc68042::error::spi_read);
  }

  SUBCASE("Read register group") {
    etl::array<uint8_t, 8> start_array0{0xAC, 0x78, 0xE0, 0x15, 0x68, 0xFF};
    uint16_t arr_pec = crc.calculate({start_array0.begin(), 6});
    start_array0[6]  = arr_pec >> 8;
    start_array0[7]  = arr_pec & 0xFF;

    stream.read_buffer.insert(stream.read_buffer.end(), start_array0.rbegin(), start_array0.rend());
    auto result = ltc.read_reg_group(3, ltc68042::reg_group_r::aux_b);
    CHECK_FALSE(stream.started);
    REQUIRE(result);
    CHECK_EQ(stream.read_buffer.size(), 0);

    auto& regs = result.value();
    for (size_t i{0}; i < regs.size(); ++i) {
      CHECK_EQ(regs[i], start_array0[i]);
    }

    REQUIRE_EQ(stream.write_buffer.size(), 4);
    constexpr uint16_t expected_cmd{0b10011'00000001110};
    etl::array<uint8_t, 2> temp = {expected_cmd >> 8, expected_cmd & 0xFF};
    const uint16_t expected_pec = crc.calculate(temp);

    uint16_t cmd = stream.write_buffer[0] << 8 | stream.write_buffer[1];
    uint16_t pec = stream.write_buffer[2] << 8 | stream.write_buffer[3];
    CHECK_EQ(cmd, expected_cmd);
    CHECK_EQ(pec, expected_pec);
  }

  SUBCASE("Write register group") {
    const etl::array<uint8_t, 6> data{0xAC, 0x78, 0xE0, 0x15, 0x68, 0xFF};
    const uint16_t expected_data_pec = crc.calculate(data);

    auto result = ltc.write_reg_group(3, data);
    CHECK_FALSE(stream.started);
    REQUIRE(result);
    REQUIRE_EQ(stream.write_buffer.size(), 12);

    constexpr uint16_t expected_cmd{0b10011'00000000001};
    etl::array<uint8_t, 2> temp = {expected_cmd >> 8, expected_cmd & 0xFF};
    const uint16_t expected_pec = crc.calculate(temp);

    uint16_t cmd = stream.write_buffer[0] << 8 | stream.write_buffer[1];
    uint16_t pec = stream.write_buffer[2] << 8 | stream.write_buffer[3];
    CHECK_EQ(cmd, expected_cmd);
    CHECK_EQ(pec, expected_pec);

    for (size_t i{0}; i < data.size(); ++i) {
      CHECK_EQ(stream.write_buffer[i + 4], data[i]);
    }
    pec = stream.write_buffer[10] << 8 | stream.write_buffer[11];
    CHECK_EQ(pec, expected_data_pec);
  }
}
