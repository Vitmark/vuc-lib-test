#include <doctest.h>

#include <cstddef>
#include <cstdint>

#include "etl/array.h"
#include "etl/endianness.h"
#include "etl/span.h"
#include "vuc/process_can.hpp"

namespace can = vuc::can;

TEST_CASE("Fill frame and parse message") {
  using T1 = uint16_t;
  using T2 = float;
  using T3 = int16_t;

  const T1 test_val_1 = 0xF5A4;
  const can::can_data test_val_raw_big1{0xF5, 0xA4, 0, 0, 0, 0, 0, 0};
  const can::can_data test_val_raw_little1{0xA4, 0xF5, 0, 0, 0, 0, 0, 0};

  const T2 test_val_2 = 0.268;
  const can::can_data test_val_raw_big2{0xF5, 0xA4, 0x3E, 0x89, 0x37, 0x4C, 0, 0};
  const can::can_data test_val_raw_little2{0xA4, 0xF5, 0x4C, 0x37, 0x89, 0x3E, 0, 0};

  const T3 test_val_3 = -8456;
  const can::can_data test_val_raw_big3{0xF5, 0xA4, 0x3E, 0x89, 0x37, 0x4C, 0xDE, 0xF8};
  const can::can_data test_val_raw_little3{0xA4, 0xF5, 0x4C, 0x37, 0x89, 0x3E, 0xF8, 0xDE};

  constexpr uint32_t message_id = 0x03A2;

  can::can_id message{message_id};

  SUBCASE("Fill frame") {
    can::can_frame frame{message, 0, {}};

    SUBCASE("Endian little, 1 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::little, test_val_1), sizeof(test_val_1));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1));
    }

    SUBCASE("Endian big, 1 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::big, test_val_1), sizeof(test_val_1));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1));
    }

    SUBCASE("Endian little, 2 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::little, test_val_1, test_val_2), sizeof(T1) + sizeof(T2));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little2[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1, test_val_2));
    }

    SUBCASE("Endian big, 2 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::big, test_val_1, test_val_2), sizeof(T1) + sizeof(T2));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big2[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1, test_val_2));
    }

    SUBCASE("Endian little, 3 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::little, test_val_1, test_val_2, test_val_3), sizeof(T1) + sizeof(T2) + sizeof(T3));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little3[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2) + sizeof(T3));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1, test_val_2, test_val_3));
    }

    SUBCASE("Endian native, 3 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame(frame, test_val_1, test_val_2, test_val_3), sizeof(T1) + sizeof(T2) + sizeof(T3));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little3[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2) + sizeof(T3));
      CHECK_EQ(frame, can::create_can_frame(message, test_val_1, test_val_2, test_val_3));
    }

    SUBCASE("Endian big, 3 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::big, test_val_1, test_val_2, test_val_3), sizeof(T1) + sizeof(T2) + sizeof(T3));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big3[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2) + sizeof(T3));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1, test_val_2, test_val_3));
    }
  }

  SUBCASE("Parse frame") {
    SUBCASE("Endian little, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_little1, etl::endian::little, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian big, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_big1, etl::endian::big, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian little, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_little2, etl::endian::little, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }

    SUBCASE("Endian big, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_big2, etl::endian::big, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }

    SUBCASE("Endian little, 3 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      T3 out_var_3{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_little3, etl::endian::little, out_var_1, out_var_2, out_var_3));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
      CHECK_EQ(out_var_3, test_val_3);
    }

    SUBCASE("Endian big, 3 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      T3 out_var_3{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_big3, etl::endian::big, out_var_1, out_var_2, out_var_3));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
      CHECK_EQ(out_var_3, test_val_3);
    }

    SUBCASE("Endian native, 3 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      T3 out_var_3{0};
      REQUIRE(can::parse_can_data(test_val_raw_little3, out_var_1, out_var_2, out_var_3));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
      CHECK_EQ(out_var_3, test_val_3);
    }
  }
}

TEST_CASE("Fill and parse frame with arrays") {
  using T1 = etl::array<uint16_t, 2>;
  using T2 = etl::array<int8_t, 3>;

  const T1 test_val_1{0xF5A4, 0xB6B2};
  const etl::span test_val_1_span{test_val_1};
  const size_t test_val_1_size = T1::SIZE * sizeof(T1::value_type);
  const can::can_data test_val_raw_big1{0xF5, 0xA4, 0xB6, 0xB2, 0, 0, 0, 0};
  const can::can_data test_val_raw_little1{0xA4, 0xF5, 0xB2, 0xB6, 0, 0, 0, 0};

  const T2 test_val_2{-25, 93, -118};
  const etl::span test_val_2_span{test_val_2};
  const size_t test_val_2_size = T2::SIZE * sizeof(T2::value_type);
  const can::can_data test_val_raw_big2{0xF5, 0xA4, 0xB6, 0xB2, 0xE7, 0x5D, 0x8A, 0};
  const can::can_data test_val_raw_little2{0xA4, 0xF5, 0xB2, 0xB6, 0xE7, 0x5D, 0x8A, 0};

  constexpr uint32_t message_id = 0x03A2;
  can::can_id message{message_id};

  SUBCASE("Fill") {
    can::can_frame frame{message, 0, {}};

    SUBCASE("Endian little, 1 variable") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::little, test_val_1), test_val_1_size);
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1_span));
    }

    SUBCASE("Endian big, 1 variable") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::big, test_val_1), test_val_1_size);
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1_span));
    }

    SUBCASE("Endian little, 2 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::little, test_val_1, test_val_2), test_val_1_size + test_val_2_size);
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little2[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1, test_val_2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::little, test_val_1_span, test_val_2_span));
    }

    SUBCASE("Endian big, 2 variables") {
      REQUIRE_EQ(can::serialize_to_can_frame_endian(frame, etl::endian::big, test_val_1, test_val_2), test_val_1_size + test_val_2_size);
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big2[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1, test_val_2));
      CHECK_EQ(frame, can::create_can_frame_endian(message, etl::endian::big, test_val_1_span, test_val_2_span));
    }
  }

  SUBCASE("Parse") {
    SUBCASE("Endian little, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_little1, etl::endian::little, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian big, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_big1, etl::endian::big, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian little, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_little2, etl::endian::little, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }

    SUBCASE("Endian big, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_endian(test_val_raw_big2, etl::endian::big, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }
  }
}

TEST_CASE("Fill and parse frame with offset") {
  using T1 = etl::array<uint16_t, 2>;
  using T2 = etl::array<int8_t, 3>;

  const T1 test_val_1{0xF5A4, 0xB6B2};
  const size_t offset_1 = 2;
  const can::can_data test_val_raw_big1{0, 0, 0xF5, 0xA4, 0xB6, 0xB2, 0, 0};
  const can::can_data test_val_raw_little1{0, 0, 0xA4, 0xF5, 0xB2, 0xB6, 0, 0};

  const T2 test_val_2{-25, 93, -118};
  const size_t offset_2 = 1;
  const can::can_data test_val_raw_big2{0, 0xF5, 0xA4, 0xB6, 0xB2, 0xE7, 0x5D, 0x8A};
  const can::can_data test_val_raw_little2{0, 0xA4, 0xF5, 0xB2, 0xB6, 0xE7, 0x5D, 0x8A};

  SUBCASE("Parse") {
    SUBCASE("Endian little, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_offset(test_val_raw_little1, etl::endian::little, offset_1, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian big, 1 variables") {
      T1 out_var_1{0};
      REQUIRE(can::parse_can_data_offset(test_val_raw_big1, etl::endian::big, offset_1, out_var_1));
      CHECK_EQ(out_var_1, test_val_1);
    }

    SUBCASE("Endian little, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_offset(test_val_raw_little2, etl::endian::little, offset_2, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }

    SUBCASE("Endian big, 2 variables") {
      T1 out_var_1{0};
      T2 out_var_2{0};
      REQUIRE(can::parse_can_data_offset(test_val_raw_big2, etl::endian::big, offset_2, out_var_1, out_var_2));
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(out_var_2, test_val_2);
    }
  }
}

TEST_CASE("Push and pop frame") {
  using T1 = uint16_t;
  using T2 = float;
  using T3 = int16_t;

  const T1 test_val_1 = 0xF5A4;
  const can::can_data test_val_raw_big1{0xF5, 0xA4, 0, 0, 0, 0, 0, 0};
  const can::can_data test_val_raw_little1{0xA4, 0xF5, 0, 0, 0, 0, 0, 0};

  const T2 test_val_2 = 0.268;
  // const can::can_data test_val_raw_big2{0xF5, 0xA4, 0x3E, 0x89, 0x37, 0x4C, 0, 0};
  // const can::can_data test_val_raw_little2{0xA4, 0xF5, 0x4C, 0x37, 0x89, 0x3E, 0, 0};

  const T3 test_val_3 = -8456;
  const can::can_data test_val_raw_big3{0xF5, 0xA4, 0x3E, 0x89, 0x37, 0x4C, 0xDE, 0xF8};
  const can::can_data test_val_raw_little3{0xA4, 0xF5, 0x4C, 0x37, 0x89, 0x3E, 0xF8, 0xDE};

  constexpr uint32_t message_id = 0x03A2;

  can::can_id message{message_id};

  SUBCASE("Fill frame") {
    can::can_frame frame{message, 0, {}};

    SUBCASE("Endian little") {
      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::little, test_val_1), sizeof(T1));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));

      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::little, test_val_2, test_val_3), sizeof(T2) + sizeof(T3));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_little3[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2) + sizeof(T3));

      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::little, test_val_1), 0);
    }

    SUBCASE("Endian big") {
      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::big, test_val_1), sizeof(T1));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big1[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1));

      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::big, test_val_2, test_val_3), sizeof(T2) + sizeof(T3));
      for (size_t i{0}; i < frame.data.size(); ++i) {
        CHECK_EQ(frame.data[i], test_val_raw_big3[i]);
      }
      CHECK_EQ(message, frame.id);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2) + sizeof(T3));

      REQUIRE_EQ(can::push_to_frame_endian(frame, etl::endian::big, test_val_1), 0);
    }
  }

  SUBCASE("Parse frame") {
    SUBCASE("Endian little") {
      can::can_frame frame{message, sizeof(T1) + sizeof(T2) + sizeof(T3), test_val_raw_little3};
      T3 out_var_3{0};
      REQUIRE(can::pop_from_can_frame_endian(frame, etl::endian::little, out_var_3));
      CHECK_EQ(out_var_3, test_val_3);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      T2 out_var_2{0};
      T1 out_var_1{0};
      REQUIRE(can::pop_from_can_frame_endian(frame, etl::endian::little, out_var_1, out_var_2));
      CHECK_EQ(out_var_3, test_val_3);
      CHECK_EQ(out_var_2, test_val_2);
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(frame.len, 0);
    }

    SUBCASE("Endian big") {
      can::can_frame frame{message, sizeof(T1) + sizeof(T2) + sizeof(T3), test_val_raw_big3};
      T3 out_var_3{0};
      REQUIRE(can::pop_from_can_frame_endian(frame, etl::endian::big, out_var_3));
      CHECK_EQ(out_var_3, test_val_3);
      CHECK_EQ(frame.len, sizeof(T1) + sizeof(T2));
      T2 out_var_2{0};
      T1 out_var_1{0};
      REQUIRE(can::pop_from_can_frame_endian(frame, etl::endian::big, out_var_1, out_var_2));
      CHECK_EQ(out_var_3, test_val_3);
      CHECK_EQ(out_var_2, test_val_2);
      CHECK_EQ(out_var_1, test_val_1);
      CHECK_EQ(frame.len, 0);
    }
  }
}
