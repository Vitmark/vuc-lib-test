#include <doctest.h>

#include <cstdint>
#include <iostream>

#include "vuc/timing.hpp"

TEST_CASE("Test time_keeper") {
  vuc::time_keeper<uint16_t, false> refresh_rate(100);

  SUBCASE("Correctly says when it needs update") {
    CHECK_FALSE(refresh_rate.needs_update(99));
    CHECK(refresh_rate.needs_update(100));
  }

  SUBCASE("Correctly saves timestamp") {
    CHECK(refresh_rate.update(105));
    CHECK_FALSE(refresh_rate.update(204));
    CHECK(refresh_rate.update(205));
    CHECK_FALSE(refresh_rate.update(304));
    CHECK(refresh_rate.update(305));
  }

  SUBCASE("Clear timestamp") {
    refresh_rate.update(105);
    refresh_rate.reset();
    CHECK(refresh_rate.update(105));
    refresh_rate.reset(110);
    CHECK_FALSE(refresh_rate.update(209));
    CHECK(refresh_rate.update(210));
  }

  SUBCASE("Reset new period") {
    refresh_rate.update(105);
    refresh_rate.reset_new_period(200);
    CHECK_FALSE(refresh_rate.update(109));
    CHECK(refresh_rate.update(200));
  }

  SUBCASE("Change update time") {
    refresh_rate.update(105);
    refresh_rate.set_update_period(200);
    CHECK_FALSE(refresh_rate.update(205));
    CHECK_FALSE(refresh_rate.update(304));
    CHECK(refresh_rate.update(305));
  }

  SUBCASE("0 update time") {
    refresh_rate.update(105);
    refresh_rate.set_update_period(0);
    CHECK(refresh_rate.update(0));
    CHECK(refresh_rate.update(205));
    CHECK(refresh_rate.update(5000));
  }

  SUBCASE("Overflow") {
    uint16_t temp = UINT16_MAX - 9;
    refresh_rate.reset(temp);
    CHECK_FALSE(refresh_rate.update(89));
    CHECK(refresh_rate.update(90));
  }
}

TEST_CASE("Test time_keeper with clearing") {
  vuc::time_keeper<uint16_t, true> refresh_rate(250, 3000);

  SUBCASE("Basic") {
    CHECK_FALSE(refresh_rate.update(3249));
    CHECK(refresh_rate.update(3250));

    CHECK(refresh_rate.update(5000));
    CHECK(refresh_rate.update(0));
  }

  SUBCASE("Reset timestamp") {
    refresh_rate.update(3000);

    refresh_rate.reset(2000);

    CHECK_FALSE(refresh_rate.update(2249));
    CHECK(refresh_rate.update(2250));

    CHECK(refresh_rate.update(5000));
    CHECK(refresh_rate.update(0));

    refresh_rate.reset(0);

    CHECK_FALSE(refresh_rate.update(249));
    CHECK(refresh_rate.update(250));
    CHECK(refresh_rate.update(0));
  }

  SUBCASE("Reset timestamp and refresh time") {
    refresh_rate.reset_new_period(180, 5000);

    CHECK_FALSE(refresh_rate.update(5179));
    CHECK(refresh_rate.update(5180));

    CHECK(refresh_rate.update(10000));
    CHECK(refresh_rate.update(0));
  }
}

TEST_CASE("Test time_keeper with overshoot correction") {
  vuc::time_keeper<uint16_t, false, true> refresh_rate(100);

  SUBCASE("Correctly says when it needs update") {
    CHECK_FALSE(refresh_rate.needs_update(99));
    CHECK(refresh_rate.needs_update(100));
    CHECK(refresh_rate.needs_update(200));
  }

  SUBCASE("Correctly increments timestamp") {
    CHECK(refresh_rate.update(105));
    CHECK_FALSE(refresh_rate.update(199));
    CHECK(refresh_rate.update(200));
    CHECK_FALSE(refresh_rate.update(299));
    CHECK(refresh_rate.update(305));
    CHECK(refresh_rate.update(400));
  }

  SUBCASE("Correctly handles overshoot") {
    CHECK(refresh_rate.update(305));        // 100
    CHECK(refresh_rate.update(310));        // 200
    CHECK(refresh_rate.update(315));        // 300
    CHECK_FALSE(refresh_rate.update(399));  // 400
    CHECK(refresh_rate.update(400));
  }

  SUBCASE("Clear timestamp") {
    refresh_rate.update(105);
    refresh_rate.reset();
    CHECK(refresh_rate.update(105));
    refresh_rate.reset(110);
    CHECK_FALSE(refresh_rate.update(209));
    CHECK(refresh_rate.update(210));
  }

  SUBCASE("Reset new period") {
    refresh_rate.update(105);
    refresh_rate.reset_new_period(200);
    CHECK_FALSE(refresh_rate.update(109));
    CHECK(refresh_rate.update(200));
  }

  SUBCASE("Change update time") {
    refresh_rate.update(105);
    refresh_rate.set_update_period(200);
    CHECK_FALSE(refresh_rate.update(205));
    CHECK_FALSE(refresh_rate.update(299));
    CHECK(refresh_rate.update(300));
  }

  SUBCASE("0 update time") {
    refresh_rate.update(105);
    refresh_rate.set_update_period(0);
    CHECK(refresh_rate.update(0));
    CHECK(refresh_rate.update(205));
    CHECK(refresh_rate.update(5000));
  }

  SUBCASE("Overflow") {
    uint16_t temp = UINT16_MAX - 9;
    refresh_rate.reset(temp);
    CHECK_FALSE(refresh_rate.update(89));
    CHECK(refresh_rate.update(90));
  }
}
